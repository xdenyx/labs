package com.company;

import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите номер числа Фибоначчи: ");
        long n = scanner.nextLong();

        CompletableFuture<Long> fibonacciFuture = CompletableFuture.supplyAsync(() -> fibonacci(n));

        System.out.println("Начало процесс...");
        while (!fibonacciFuture.isDone()) {
            System.out.println("Ожидание...");
            Thread.sleep(500);
        }
        long result = fibonacciFuture.get();
        System.out.println("Результат: " + result);
    }

    public static long fibonacci(long n) {
        if (n <= 1) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}

import java.io.File;

public class DirectoryTree {
    public static void main(String[] args) {
        File currentDirectory = new File(".");
        printDirectoryTree(currentDirectory, "");
    }

    public static void printDirectoryTree(File folder, String indent) {
        System.out.println(indent + folder.getName());
        indent += "\t";

        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    printDirectoryTree(file, indent);
                } else {
                    System.out.println(indent + file.getName());
                }
            }
        }
    }
}

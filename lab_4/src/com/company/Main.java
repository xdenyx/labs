package com.company;

import java.util.Random;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        int[] input1 = generateRandomArray(10000);
        int[] input2 = generateRandomArray(10000);

        // синхронный вариант (sleep 0)
        long time1_0 = SyncVariant(input1, input2, 0);
        System.out.printf("Синхронний вариант(sleep 0): %d мс\n", time1_0);

        // синхронный вариант (sleep 1)
        long time1_1 = SyncVariant(input1, input2, 1);
        System.out.printf("Синхронний вариант(sleep 1): %d мс\n", time1_1);

        // parallel stream вариант (sleep 0)
        long time2_0 = ParallelStreamVariant(input1, input2, 0);
        System.out.printf("Parallel stream вариант(sleep 0): %d мс\n", time2_0);

        // parallel stream вариант (sleep 1)
        long time2_1 = ParallelStreamVariant(input1, input2, 1);
        System.out.printf("Parallel stream вариант(sleep 1): %d мс\n", time2_1);
    }
    // синхронный вариант
    private static long SyncVariant(int[] input1, int[] input2, int sleep) {
        long time = System.currentTimeMillis();
        int[] result = new int[input1.length];
        for (int i = 0; i < input1.length; i++) {
            result[i] = input1[i] * input2[i];
            sleep_(sleep);
        }
        return System.currentTimeMillis() - time;
    }
    // parallel stream вариант
    private static long ParallelStreamVariant(int[] input1, int[] input2, int sleep) {
        long time = System.currentTimeMillis();
        int[] result = IntStream.range(0, input1.length).parallel().map(i -> {
            sleep_(sleep);
            return input1[i] * input2[i];
        }).toArray();
        return System.currentTimeMillis() - time;
    }

    private static void sleep_(int sleep) {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static int[] generateRandomArray(int length) {
        Random random = new Random();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = random.nextInt(101); // Random numbers from 0 to 100
        }
        return array;
    }
}
